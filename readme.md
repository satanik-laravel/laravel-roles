# Satanik Roles Package

[![Software License][ico-license]](LICENSE.md)

This package adds roles for users.

### Dependencies

* satanik/foundation - [git@gitlab.com:satanik/laravel-foundation.git]()

## Structure

The directory structure follows the industry standard.

```bash
config/
src/
tests/
```


## Install

Via Composer

``` bash
$ composer require satanik/roles
```

if the package is not published use this in the composer file

```json
"repositories": {
  "satanik/roles": {
    "type": "vcs",
    "url": "git@gitlab.com:satanik/laravel-roles.git"
  },
  ...
},
...
"require": {
  ...
  "satanik/roles": "<version constraint>",
```

or copy the repository to

```bash
<project root>/packages/Satanik/Roles
```

and use this code in the composer file

```json
"repositories": {
  "satanik/roles": {
    "type": "path",
    "url": "packages/Satanik/Roles",
    "options": {
      "symlink": true
    }
  },
  ...
},
...
"require": {
  ...
  "satanik/roles": "@dev",
```

## Usage

###Roles

This package adds a `roles` table to the database on `php artisan migrate`.

### HasRoles Trait

To use the roles from the use side add the `HasRoles trait to the main user model.

```php
namespace App;

use Satanik\Roles\Concerns\HasRole;

class User extends Authenticatable
{
    use HasRole;
}
```

The roles can be accessed then by using

```php
Auth::user()->role()->name;
Auth::user()->role()->displayname;
```

## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email daniel@satanik.at instead of using the issue tracker.

## Credits

- [Daniel Satanik][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-author]: https://satanik.at
