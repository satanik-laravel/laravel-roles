<?php

namespace Satanik\Roles\Concerns;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use Satanik\Roles\Models\Role;

trait HasRole
{
    /**
     * @return BelongsTo|Collection|Role|self
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
}
