<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 05.04.18
 * Time: 17:12
 */

namespace Satanik\Roles\Models;

use Satanik\Foundation\Abstraction\User;
use Satanik\Foundation\Abstraction\Model;

/**
 * Satanik\Foundation\Models\Role
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string $displayname
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\Model joinModel($model, $one, $operator = null, $two = null, $type = 'inner', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\Model joinPivot($table)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\Model nPerGroup($group, $number = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\Model top($number = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Roles\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Roles\Models\Role whereDisplayname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Roles\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Roles\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Roles\Models\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Role extends Model
{
    protected $fillable = ['name', 'displayname'];

    public function users()
    {
        $user_model = User::bound();

        return $this->belongsToMany($user_model, 'user_roles')
                    ->select(app($user_model)->getTable() . '.*')
                    ->union($this->hasMany($user_model))->getQuery();
    }
}
